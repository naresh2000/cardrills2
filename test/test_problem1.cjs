const get_car_id = require('../problem1.cjs');
const inventory = require('../inventory.cjs'); 

const result = get_car_id(inventory,33);

console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);

