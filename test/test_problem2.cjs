const problem2 = require('../problem2.cjs');
const inventory = require('../inventory.cjs');

const lastCar = problem2(inventory);
console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);